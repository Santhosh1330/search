export default ({ markup, helmet }) => {
	return `<!doctype html>
<html ${helmet.htmlAttributes.toString()}>
<head>
	${helmet.title.toString()}
	${helmet.meta.toString()}
	${helmet.link.toString()}
</head>
<body ${helmet.bodyAttributes.toString()}>
	<div id="root">${markup}</div>
	<script src="/static/client.js" async></script>
	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="%PUBLIC_URL%/developer.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.carousel .carousel-item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length > 0) {
            next.next().children(':first-child').clone().appendTo($(this));
        } else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });
var liSelected;
var content;
$('#myForm').keydown(function(e) {
    if(e.which === 40) {
        if(liSelected) {
            liSelected.removeClass('autosuggestactive');
            next = liSelected.next();
            if(next.length > 0) {
                liSelected = next.addClass('autosuggestactive');
                content = $('li.autosuggestactive').text();
                $('input.search-query').val(content);
            } else {
                liSelected = $('ul.autosuggest li').eq(0).addClass('autosuggestactive');
                content = $('li.autosuggestactive').text();
                $('input.search-query').val(content);
            }
        } else {
            liSelected = $('ul.autosuggest li').eq(0).addClass('autosuggestactive');
            content = $('li.autosuggestactive').text();
                $('input.search-query').val(content);
        }
    } else if(e.which === 38) {
        if(liSelected) {
            liSelected.removeClass('autosuggestactive');
            next = liSelected.prev();
            if(next.length > 0) {
                liSelected = next.addClass('autosuggestactive');
                content = $('li.autosuggestactive').text();
                $('input.search-query').val(content);
            } else {
                liSelected = $('ul.autosuggest li').last().addClass('autosuggestactive');
                content = $('li.autosuggestactive').text();
                $('input.search-query').val(content);
            }
        } else {
            liSelected = $('ul.autosuggest li').last().addClass('autosuggestactive');
            content = $('li.autosuggestactive').text();
            $('input.search-query').val(content);
        }
    }
});
  });
</script>
</body>
</html>`;
};