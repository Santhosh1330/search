import React,{Component} from 'react'
import { ShareButtons, ShareCounts, generateShareIcon } from 'react-share'

const {
  FacebookShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  } = ShareButtons;

const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const GooglePlusIcon = generateShareIcon('google');
const LinkedinIcon = generateShareIcon('linkedin');
const WhatsappIcon = generateShareIcon('whatsapp');

export default class Footer extends Component{
        
render(){
    const shareUrl = 'https://searchello.com/';
    const title = 'Searchello';
            return(
                <div>
                <footer className="foo-border home-footer hidden-lg-down">
                    <div className="container">
                            <div className="row flex-column">
                                <div className="d-flex justify-content-around flex-wrap footer-content text-center">
                                    <small className="p-4 hidden-xs-down">&copy;Copyright. All rights reserved.</small>
                                      <div className="p-2 mr-md-5 footer-content float-md-left">
                                        <small>Share to make the world safer:</small>
                                        <div className="row d-flex  justify-content-around mt-2 mb-1">
                                            <FacebookShareButton url={shareUrl} quote={title} className="Demo__some-network__share-button">
                                                <FacebookIcon size={26} round />
                                            </FacebookShareButton>
                                            <GooglePlusShareButton url={shareUrl} className="Demo__some-network__share-button"> 
                                                <GooglePlusIcon size={26} round />
                                            </GooglePlusShareButton>
                                            <TwitterShareButton url={shareUrl} title={title} className="Demo__some-network__share-button">
                                                <TwitterIcon size={26} round />
                                            </TwitterShareButton>
                                            <LinkedinShareButton url={shareUrl} title={title} windowWidth={750} windowHeight={600} className="Demo__some-network__share-button">
                                                <LinkedinIcon size={26} round />
                                            </LinkedinShareButton>
                                            <WhatsappShareButton url={shareUrl} title={title} separator=":: " className="Demo__some-network__share-button">
                                                <WhatsappIcon size={26} round />
                                            </WhatsappShareButton>
                                        </div>
                                       {/* <div className="powered text-center mt-lg-3">
                                            <small>
                                                Powered by
                                                <img src={require("../../stylesheet/image/bing-logo.png")} style={{width:"13px"}}/>
                                            </small>
                                        </div> */}
                                    </div>
                                    <small className="p-4 hidden-xs-down">
                                        <a className="footer-partner p-2" href="partnerus">Partner with us</a>
                                    </small>
                                </div>
                            </div>
                        </div>
                </footer>
                <footer className="foo-border home-footer hidden-xl-up">
                    <div className="container">
                            <div className="row text-center">
                                <div className="col-md-12 py-4">
                                    <small>
                                      <a href="" className="footer-download-app px-3 py-2">
                                      <i aria-hidden="true" className="fa fa-download pr-2"></i>Download App</a>
                                    </small>
                                </div>
                                <div className="col-md-12 footer-content float-md-left">
                                    <small>Share to make the world safer:</small>
                                        <div className="row d-flex  justify-content-center mt-2 mb-1">
                                            <FacebookShareButton url={shareUrl} quote={title} className="Demo__some-network__share-button mx-1">
                                                <FacebookIcon size={26} round />
                                            </FacebookShareButton>
                                            <GooglePlusShareButton url={shareUrl} className="Demo__some-network__share-button mx-1"> 
                                                <GooglePlusIcon size={26} round />
                                            </GooglePlusShareButton>
                                            <TwitterShareButton url={shareUrl} title={title} className="Demo__some-network__share-button mx-1">
                                                <TwitterIcon size={26} round />
                                            </TwitterShareButton>
                                            <LinkedinShareButton url={shareUrl} title={title} windowWidth={750} windowHeight={600} className="Demo__some-network__share-button mx-1">
                                                <LinkedinIcon size={26} round />
                                            </LinkedinShareButton>
                                            <WhatsappShareButton url={shareUrl} title={title} separator=":: " className="Demo__some-network__share-button mx-1">
                                                <WhatsappIcon size={26} round />
                                            </WhatsappShareButton>
                                        </div>
                                </div>
                                <div className="col-md-12 py-2">
                                   <small>&copy; 2017 Searchello</small>
                                </div>
                            </div>
                        </div>
                </footer>
                </div>
                    );
        }
}