import React,{Component} from 'react'
import Footer from "./common/Footer"
import Cookies from 'universal-cookie'
import {Helmet} from "react-helmet"

export default class Search extends Component{
    constructor(props){
        super(props)
    }

    componentDidMount(){
        const cookies = new Cookies()
        let checkCokkie = cookies.getAll()
        let expiry = new Date(new Date().getTime()+60*60*1000*24*365)
        fetch("http://freegeoip.net/json/")
            .then(response => response.json())
            .then((array)=>{
                if(document.cookie.indexOf('gc=') == '-1'){                    
                    cookies.set('gc', array.country_code , {expires  : expiry})
                    cookies.set('cn', array.country_name, {expires  : expiry})
                }
                console.log(array)
            })
        if(document.cookie.indexOf('gc=') == '-1'){
            let searchvalue = (cookies.get('ns') != '0')?cookies.get('ns'):'0'
            cookies.set('t',false, {expires  : expiry})
            cookies.set('s',true, {expires  : expiry})
            cookies.set('p',true, {expires  : expiry})
            cookies.set('n',true, {expires  : expiry})
            cookies.set('ns',searchvalue, {expires  : expiry})
        }
    }


    render(){
         return(
                <div>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>Searchello - Safe Search Engine | Search to make the world safer</title>
                    <meta name="description" content="Searchello is a safe search engine, which shows search results that are safe for our society and community. Switch to Searchello and make the world safer." />
                </Helmet>  
                
                <Footer />
                </div>
            );
        
    }
}